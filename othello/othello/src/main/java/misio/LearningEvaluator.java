package misio;

import put.ci.cevo.games.othello.OthelloBoard;
import put.ci.cevo.games.player.BoardMoveEvaluator;

public interface LearningEvaluator extends BoardMoveEvaluator<OthelloBoard>{
	public void startGame();
	public void endGame(double points);
	public String getStrategy();
	public void moveSelected(OthelloBoard board, int move, int player);
}
