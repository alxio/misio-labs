package misio;

import java.util.ArrayList;
import java.util.List;

import put.ci.cevo.games.board.Board;
import put.ci.cevo.games.othello.OthelloBoard;
import put.ci.cevo.games.othello.players.OthelloPlayer;
import put.ci.cevo.games.player.BoardMoveEvaluator;

import com.google.common.base.Preconditions;

public class FinalMoveEvaluator implements LearningEvaluator {
	private double[] values =

	{ 0.08614517028837532, -0.011802771875235414, 0.008812571167242756,
			-0.0014407061174916335, -0.0018132577602282926,
			0.005018965698787516, -0.00717390333679816, 0.0708848387330005,
			-0.01703843115280995, -0.009009580881949697, -0.002091666781391678,
			0.0010829022322018705, -0.003888596767707652,
			-0.002951015427069489, -0.005266681536753709, -0.01985906911082652,
			0.017804992855137366, -0.0016343080853635627,
			-0.0031898138770094877, -0.007934954396642071,
			-0.002039593014289368, 7.602251445812242E-4, 0.0068007683008427565,
			0.007671404249022741, -0.00791685583028192, 0.012184376069807632,
			-0.013510271750744547, -0.05220739527538845, -0.043809217799414844,
			-0.003945199432476456, -0.0011497040121440727,
			-0.001522469550901328, -0.0015410898837341668,
			0.008694523950634032, 0.036494055049633656, 0.032621646592023906,
			0.01172248000036917, 0.008618860421848042, 0.005647467652667037,
			-0.004317250633039147, 0.010849658751967726, -9.097547479220851E-4,
			0.017348371253650744, 0.041698901815100844, 0.043095776632499846,
			0.030290093246627563, 0.009656075227790271, 0.005423723459426936,
			-0.004580142044653616, -0.00890166695090669, -0.00419634094763055,
			0.006274407643478897, 0.017215904550117157, -0.0015036157855770177,
			-0.006759440542899154, -0.0044366696702059305, 0.08322599428202324,
			-0.006326912298443412, 0.029136393391207428, -0.002449505230474282,
			-0.0038952165060715735, 0.028793230115829467,
			-0.009824280248477103, 0.07834337720348775 };

	// new double[64];

	// {1.00,-0.25, 0.10, 0.05, 0.05, 0.10,-0.25, 1.00,
	// -0.25,-0.25, 0.01, 0.01, 0.01, 0.01,-0.25,-0.25,
	// 0.10, 0.01, 0.05, 0.02, 0.02, 0.05, 0.01, 0.10,
	// 0.05, 0.01, 0.02, 0.01, 0.01, 0.02, 0.01, 0.05,
	// 0.05, 0.01, 0.02, 0.01, 0.01, 0.02, 0.01, 0.05,
	// 0.10, 0.01, 0.05, 0.02, 0.02, 0.05, 0.01, 0.10,
	// -0.25,-0.25, 0.01, 0.01, 0.01, 0.01,-0.25,-0.25,
	// 1.00,-0.25, 0.10, 0.05, 0.05, 0.10,-0.25, 1.00};

	// {1.01,-0.43, 0.38, 0.07, 0.00, 0.42, -0.20, 1.02,
	// -0.27,-0.74,-0.16,-0.14,-0.13,-0.25, -0.65,-0.39,
	// 0.56,-0.30, 0.12, 0.05,-0.04, 0.07, -0.15, 0.48,
	// 0.01,-0.08, 0.01,-0.01,-0.04,-0.02, -0.12, 0.03,
	// -0.10,-0.08, 0.01,-0.01,-0.03, 0.02, -0.04,-0.20,
	// 0.59,-0.23, 0.06, 0.01, 0.04, 0.06, -0.19, 0.35,
	// -0.06,-0.55,-0.18,-0.08,-0.15,-0.31, -0.82,-0.58,
	// 0.96,-0.42, 0.67,-0.02,-0.03, 0.81, -0.51, 1.01};

	/**
	 * @param move
	 *            is a value rc, where (1 <= r,c <= 8), e.g. 23 means row=2,
	 *            col=3
	 * @player is a player I'm playing with
	 * @board is an Othello board with Board.WHITE, Board.BLACK or Board.EMPTY
	 *        pieces
	 */
	@Override
	public double evaluateMove(OthelloBoard board, int move, int player) {
		Preconditions.checkArgument(11 <= move && move <= 88);
		Preconditions.checkArgument(player == Board.WHITE
				|| player == Board.BLACK);
		OthelloBoard copy = board.clone();
		copy.makeMove(move, player);
		return eval(copy);
	}

	private double eval(OthelloBoard board) {
		double val = 0;
		for (int i = 0; i < 8; i++)
			for (int j = 0; j < 8; j++) {
				double field = board.getValue(i, j) - 1;
				double mult = values[i * 8 + j];
				double part = field * mult;
				double v = val;
				val += part;
			}
		return val;
	}

	@Override
	public void startGame() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endGame(double points) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getStrategy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void moveSelected(OthelloBoard board, int move, int player) {
		// TODO Auto-generated method stub
		
	}
}
