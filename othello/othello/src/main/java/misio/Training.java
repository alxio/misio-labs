package misio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.commons.math3.util.Pair;

import put.ci.cevo.games.GameOutcome;
import put.ci.cevo.games.MorePointsGameResultEvaluator;
import put.ci.cevo.games.othello.Othello;
import put.ci.cevo.games.othello.players.OthelloPlayer;
import put.ci.cevo.games.othello.players.OthelloWPCPlayer;
import put.ci.cevo.games.othello.players.wpc.LucasRunnarson2006;
import put.ci.cevo.games.othello.players.wpc.OthelloStandardWPCHeuristic;
import put.ci.cevo.games.othello.players.wpc.SzubertJaskowskiKrawiec2009;
import put.ci.cevo.games.othello.players.wpc.SzubertJaskowskiKrawiec2011;

public class Training {

	private static double EPS = 0.1;
	private static int REPEATS = 1000;
	private static int TOTAL = 250;
	
	private static Othello othello = new Othello(
			new MorePointsGameResultEvaluator(1, 0, 0.5));

	private static double playDoubleGames(OthelloPlayer firstPlayer,
			OthelloPlayer secondPlayer, int repeats, RandomDataGenerator random, LearningEvaluator evaluator) {
		double sumPoints = 0.0;
		for (int i = 0; i < repeats; ++i) {
			evaluator.startGame();
			GameOutcome first = othello.play(firstPlayer, secondPlayer, random);
			evaluator.endGame(first.blackPlayerPoints - 0.5);
			evaluator.startGame();
			GameOutcome second = othello
					.play(secondPlayer, firstPlayer, random);
			evaluator.endGame(first.whitePlayerPoints - 0.5);
			sumPoints += (first.blackPlayerPoints + second.whitePlayerPoints - 1) / 2.0;
		}
		return sumPoints / repeats;
	}

	public static void main(String[] args) {
		RandomDataGenerator random = new RandomDataGenerator(new MersenneTwister(System.currentTimeMillis()));
		ArrayList<Pair<String, OthelloPlayer>> players = new ArrayList<>();
		players.add(new Pair<String, OthelloPlayer>("SWH", new OthelloWPCPlayer(new OthelloStandardWPCHeuristic().create(), EPS)));
		players.add(new Pair<String, OthelloPlayer>("LR06", new OthelloWPCPlayer(new LucasRunnarson2006().create(), EPS)));
		players.add(new Pair<String, OthelloPlayer>("SJK09", new OthelloWPCPlayer(new SzubertJaskowskiKrawiec2009().create(), EPS)));
		players.add(new Pair<String, OthelloPlayer>("SJK11", new OthelloWPCPlayer(new SzubertJaskowskiKrawiec2011().create(), EPS)));
		players.add(new Pair<String, OthelloPlayer>("Rand1", new MISiOOthelloPlayer(new ExampleMoveEvaluator(random))));
		players.add(new Pair<String, OthelloPlayer>("Rand2", new MISiOOthelloPlayer(new ExampleMoveEvaluator(random))));
		players.add(new Pair<String, OthelloPlayer>("Rand3", new MISiOOthelloPlayer(new ExampleMoveEvaluator(random))));
		players.add(new Pair<String, OthelloPlayer>("Final", new MISiOOthelloPlayer(new FinalMoveEvaluator())));

		LearningEvaluator trainee = new TrainingMoveEvaluator(Double.parseDouble(args[0]));
		best = 0;
		while(TOTAL-- > 0){
			training(trainee, random, players, REPEATS);
		}
		System.out.print("\nBEST; " + best + "; " + bestS+"\n");
	}
	static String bestS = null;
	static double best = 0;
	
	private static void training(LearningEvaluator evaluator,
			RandomDataGenerator random,
			ArrayList<Pair<String, OthelloPlayer>> opponents, int repeats) {

		final String NAME = "Trainee";
		OthelloPlayer player = new MISiOOthelloPlayer(evaluator);
		final int n = opponents.size();
		final double results[] = new double[n + 1];
		String strategy = evaluator.getStrategy();
		for (int i = 0; i < n; i++) {
			double res = playDoubleGames(player, opponents.get(i).getSecond(),
					REPEATS, random, evaluator);
			results[n] += res / n;
			results[i] += -res;
		}

		Integer[] keys = new Integer[n + 1];
		for (int i = 0; i < n + 1; i++) {
			keys[i] = i;
		}

		Arrays.sort(keys, new Comparator<Integer>() {
			@Override
			public int compare(final Integer o1, final Integer o2) {
				double diff = results[o2] - results[o1];
				if (diff > 0) {
					return 1;
				}
				if (diff < 0) {
					return -1;
				}
				return 0;
			}
		});

		StringBuilder str = new StringBuilder();
		for (int key : keys) {
			String name = key == n ? NAME : opponents.get(key).getFirst();
			str.append(name + "\t" + results[key] + "\n");
		}
		if(results[n] > best){
			best = results[n];
			bestS = strategy;
		}
		//System.out.println(str.toString());
		//System.out.println(results[n] + "; "+ evaluator.getStrategy());
		System.out.print(results[n] + "; ");
	}
}
