package misio;

import java.util.ArrayList;
import java.util.List;

import put.ci.cevo.games.board.Board;
import put.ci.cevo.games.othello.OthelloBoard;

import com.google.common.base.Preconditions;

public class TrainingMoveEvaluator implements LearningEvaluator {
	enum Algo {
		ALL, SINGLE, ALL_WEIGHTED
	};

	final Algo algo = Algo.ALL_WEIGHTED;

	public TrainingMoveEvaluator(double a) {
		this.a = a;
		for (int i = 0; i < values.length; i++) {
			values[i] = 0.1;
		}
	}

	private List<Integer> moves = new ArrayList<Integer>();
	private List<OthelloBoard> boards = new ArrayList<OthelloBoard>();

	private double[] values = new double[64];
	// {1.00,-0.25, 0.10, 0.05, 0.05, 0.10,-0.25, 1.00,
	// -0.25,-0.25, 0.01, 0.01, 0.01, 0.01,-0.25,-0.25,
	// 0.10, 0.01, 0.05, 0.02, 0.02, 0.05, 0.01, 0.10,
	// 0.05, 0.01, 0.02, 0.01, 0.01, 0.02, 0.01, 0.05,
	// 0.05, 0.01, 0.02, 0.01, 0.01, 0.02, 0.01, 0.05,
	// 0.10, 0.01, 0.05, 0.02, 0.02, 0.05, 0.01, 0.10,
	// -0.25,-0.25, 0.01, 0.01, 0.01, 0.01,-0.25,-0.25,
	// 1.00,-0.25, 0.10, 0.05, 0.05, 0.10,-0.25, 1.00};

	// {1.01,-0.43, 0.38, 0.07, 0.00, 0.42, -0.20, 1.02,
	// -0.27,-0.74,-0.16,-0.14,-0.13,-0.25, -0.65,-0.39,
	// 0.56,-0.30, 0.12, 0.05,-0.04, 0.07, -0.15, 0.48,
	// 0.01,-0.08, 0.01,-0.01,-0.04,-0.02, -0.12, 0.03,
	// -0.10,-0.08, 0.01,-0.01,-0.03, 0.02, -0.04,-0.20,
	// 0.59,-0.23, 0.06, 0.01, 0.04, 0.06, -0.19, 0.35,
	// -0.06,-0.55,-0.18,-0.08,-0.15,-0.31, -0.82,-0.58,
	// 0.96,-0.42, 0.67,-0.02,-0.03, 0.81, -0.51, 1.01};

	private double C = 100000;

	private double a = 0.01;
	private double fade = 0.995;
	private long games = 0;

	/**
	 * @param move
	 *            is a value rc, where (1 <= r,c <= 8), e.g. 23 means row=2,
	 *            col=3
	 * @player is a player I'm playing with
	 * @board is an Othello board with Board.WHITE, Board.BLACK or Board.EMPTY
	 *        pieces
	 */
	@Override
	public double evaluateMove(OthelloBoard board, int move, int player) {
		Preconditions.checkArgument(11 <= move && move <= 88);
		Preconditions.checkArgument(player == Board.WHITE
				|| player == Board.BLACK);
		OthelloBoard copy = board.clone();
		copy.makeMove(move, player);
		return eval(copy);

	}

	private double eval(OthelloBoard board) {
		double val = 0;
		for (int i = 0; i < 8; i++)
			for (int j = 0; j < 8; j++) {
				double field = board.getValue(i, j) - 1;
				double mult = values[i * 8 + j];
				double part = field * mult;

				if (part == Double.NaN || part == Double.NEGATIVE_INFINITY
						|| part == Double.POSITIVE_INFINITY) {
					int a = 0;
				}
				double v = val;
				val += part;
				if (val == Double.NaN || val == Double.NEGATIVE_INFINITY
						|| val == Double.POSITIVE_INFINITY) {
					int a = 0;
				}
			}
		return val;
	}

	@Override
	public void startGame() {
		moves.clear();
		boards.clear();
	}

	@Override
	public void endGame(double points) {
		double[] newValues = values.clone();
		switch (algo) {
		case ALL:
			a /= 64;
			for (OthelloBoard m : boards) {
				feedback(m, points, newValues);
			}
			a *= 64;
			break;
		case SINGLE:
			for (Integer m : moves) {
				feedbackSingle(m, points, newValues);
			}
			break;
		case ALL_WEIGHTED:
			double A = a;
			int i = 1;
			for (OthelloBoard m : boards) {
				a = A * i / boards.size();
				feedback(m, points, newValues);
				i++;
			}
			a = A;
		}
		values = newValues;
		if (++games % 1000 == 0) {
			a *= fade;
		}
	}

	private void feedbackSingle(int move, double points, double[] newValues) {
		int i = 8 * (move / 10 - 1) + move % 10 - 1;
		newValues[i] += a * (points - values[i]);

		// for (int i = 0; i < 64; i++) {
		// double v = a * (points - ev) * (m.getValue(i / 8, i % 8) - 1);
		// double u = newValues[i];
		//
		// newValues[i] = (u+v) / ((1 + Math.abs(u)*Math.abs(v)) / C*C);
		// double d = newValues[i];
		// if(d == Double.NaN || d > C || d < -C){
		// int a = 0;
		// }
		// }
	}

	private void feedback(OthelloBoard m, double points, double[] newValues) {
		double ev = eval(m);
		for (int i = 0; i < 64; i++) {
			double v = a * (points - ev) * (m.getValue(i / 8, i % 8) - 1);
			double u = newValues[i];

			newValues[i] = (u + v) / ((1 + Math.abs(u) * Math.abs(v)) / C * C);
			double d = newValues[i];
			if (d == Double.NaN || d > C || d < -C) {
				int a = 0;
			}
		}
	}

	@Override
	public String getStrategy() {
		StringBuilder sb = new StringBuilder("{ ");
		for (int i = 0; i < values.length; i++) {
			sb.append(values[i]);
			sb.append(", ");
		}
		sb.replace(sb.length() - 2, sb.length(), "};");
		return sb.toString();
	}

	@Override
	public void moveSelected(OthelloBoard board, int move, int player) {
		board.makeMove(move, player);
		boards.add(board.clone());
		moves.add(move);
	}
}
