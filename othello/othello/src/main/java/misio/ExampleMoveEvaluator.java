package misio;

import org.apache.commons.math3.random.RandomDataGenerator;

import put.ci.cevo.games.board.Board;
import put.ci.cevo.games.othello.OthelloBoard;

import com.google.common.base.Preconditions;

public class ExampleMoveEvaluator implements LearningEvaluator {
	
	private RandomDataGenerator random;
	public ExampleMoveEvaluator(RandomDataGenerator random) {
		this.random = random;
	}
	/**
	 * @param move is a value rc, where (1 <= r,c <= 8), e.g. 23 means row=2, col=3 
	 * @player is a player I'm playing with
	 * @board is an Othello board with Board.WHITE, Board.BLACK or Board.EMPTY pieces
	 */
	@Override
	public double evaluateMove(OthelloBoard board, int move, int player) {
		Preconditions.checkArgument(11 <= move && move <= 88);
		Preconditions.checkArgument(player == Board.WHITE || player == Board.BLACK);
		return random.nextUniform(-1, 1);
	}
	
	@Override
	public void startGame() {
	}

	@Override
	public void endGame(double points) {
	}
	
	@Override
	public String getStrategy() {
		return "";
	}

	@Override
	public void moveSelected(OthelloBoard board, int move, int player) {
	}
}
