package misio;

import org.apache.commons.math3.random.RandomDataGenerator;

import put.ci.cevo.games.board.BoardEvaluationType;
import put.ci.cevo.games.othello.OthelloBoard;
import put.ci.cevo.games.othello.players.OthelloPlayer;
import put.ci.cevo.games.player.MoveEvaluatorPlayer;

public class MISiOOthelloPlayer implements OthelloPlayer {
	private final MoveEvaluatorPlayer<OthelloBoard> evaluatorPlayer;
	private final double RANDOM_MOVE_PROBABILITY = 0.1;
	private final LearningEvaluator evaluator;
	public MISiOOthelloPlayer(LearningEvaluator moveEvaluator) {
		evaluator = moveEvaluator;
		evaluatorPlayer = new MoveEvaluatorPlayer<>(moveEvaluator, RANDOM_MOVE_PROBABILITY, BoardEvaluationType.BOARD_INVERSION);
	}

	@Override
	public int getMove(OthelloBoard board, int player, int[] possibleMoves,	RandomDataGenerator random) {
		int move = evaluatorPlayer.getMove(board, player, possibleMoves, random);
		evaluator.moveSelected(board, move, player);
		return move;
	}
}
